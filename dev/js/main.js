$(function () {

//city select
    $('.jsToggleCitySelect').on('click', function () {
        $(this).next('.user-location__list').slideToggle('200');
        $(this).toggleClass('open');
    });

    $('.user-location__list a').on('click', function () {
        var $curretnCity = $(this).text();
        $(this).parents('.user-location__dropdown').find('.jsToggleCitySelect').text($curretnCity);
        $(this).next('.user-location__list').slideUp('200');
        $(this).parents('.user-location__dropdown').find('.jsToggleCitySelect').removeClass('open');
    });

//phone select
    $('.jsPhoneTriger').on('click', function () {
        $('.phone__dropdown').slideToggle('200');
        $(this).toggleClass('open');
    });
//dropdown reset
    $(document).click(function (e) {
        var $triger = $('.jsToggleCitySelect'),
            $dropdown = $('.user-location__list'),
            $triger1 = $('.jsPhoneTriger'),
            $dropdown1 = $('.phone__dropdown');
        if (!$triger.is(e.target) && $triger.has(e.target).length === 0) {
            $dropdown.slideUp('200');
            $triger.removeClass('open');
        }
        if (!$triger1.is(e.target) && $triger1.has(e.target).length === 0) {
            $dropdown1.slideUp('200');
            $triger1.removeClass('open');
        }
    });
// sliders
    $('.jsSliderFor').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.jsSliderNav'
    });
    $('.jsSliderNav').slick({
        slidesToShow: 10,
        slidesToScroll: 1,
        asNavFor: '.jsSliderFor',
        focusOnSelect: true,
        arrows: false
    });
    $('.jsSimilarProductSlider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        nextArrow: '<div class="slick-arrow_right"></div>',
        prevArrow: '<div class="slick-arrow_left"></div>',
        arrows: true,
        appendArrows: '.jsSimilarProductSlider_arrows',
    });
    $('.jsSliderArticles').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        nextArrow: '<div class="slick-arrow_right _left-side"></div>',
        prevArrow: '<div class="slick-arrow_left _left-side"></div>',
        arrows: true,
        variableWidth: true,
        // appendArrows: '.jsSliderArticles__arrows',
    });

    $('.jsProductsThumbsSlider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        nextArrow: '<div class="slick-arrow_right"></div>',
        prevArrow: '<div class="slick-arrow_left"></div>',
        arrows: true,
    });

    $('.jsProductsThumbsSlider_solid').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        nextArrow: '<div class="slick-arrow_right _left-side"></div>',
        prevArrow: '<div class="slick-arrow_left _left-side"></div>',
        arrows: true,
        variableWidth: true,
    });


// zoom
    $('.easyzoom').easyZoom();
    // On after slide change
    $('.jsSliderFor').on('afterChange', function (event, slick, currentSlide) {
        $('.easyzoom').easyZoom();
        $('iframe').each(function () {
            $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
        });
    });

// 3d j360
    $('.js3d').j360();

// toggle similar
    $('.jsToggleSimilar a').on('click', function () {
        $('.jsSimilarHide').toggleClass('show');
    });
// show more in tab
    $('.jsShowMore').on('click', function () {
        $(this).parents('.tab').toggleClass('show');

    });
// show more testemonials
    $('.jsShowMore._testemonials').on('click', function () {
        $(this).parents('.tab').find('.testemonial').toggleClass('open');
    });

// show answers
    $('.jsShowAnswer').on('click', function () {
        $(this).parents('.testemonial__content').find('.testemonial__answers').slideToggle('show');
    });

//smoothscroll
    $('.jsTotestemonials').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                $('#tabs li').removeClass('active');
                $('#tabs').find('[href*="menu2"]').parent().addClass('active');

                $('.sub-nav-tab li').removeClass('active');
                $('.sub-nav-tab').find('[href*="menusub0"]').parent().addClass('active');

                $('.tab-pane').removeClass('active in');
                $('#menu2, #menusub0').addClass('active in');
                return false;
            }
        }

    });
    $('.jsToquestions').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                $('#tabs li').removeClass('active');
                $('#tabs').find('[href*="menu2"]').parent().addClass('active');
                $('.tab-pane').removeClass('active in');

                $('.sub-nav-tab li').removeClass('active');
                $('.sub-nav-tab').find('[href*="menusub1"]').parent().addClass('active');

                $('#menu2, #menusub1').addClass('active in');
                return false;
            }
        }
    });
    $('.scrollTo').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    // stiky side bar

    // $(".jsSticky").stick_in_parent({
    //
    // });
});

